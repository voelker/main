# -*- coding: utf-8 -*-
"""
Created on Thu Sep  9 13:51:29 2021
Modified on Mo Nov 18 14:38 2024

@author: Sven Karsten, Georg Sebastian Voelker
"""
from __future__ import annotations

"""IOW-ESM control center"""

from PySide6.QtWidgets import QApplication
import sys
import glob

if not glob.glob('./frontend'):
    print("This script would start the graphical user interface (GUI).")
    print("The GUI is not yet installed, please pull it from the repository or use the command-line interface.")
    sys.exit()

import frontend

if __name__ == '__main__':
    app = QApplication()
    application = frontend.main.IowEsmGui()
    application.show()
    sys.exit(app.exec())
