# levante relies on a tight SLURM integration and utilizes srun
mpi_run_command = 'srun --mpi=pmi2 -n _THREADS_ --multi-prog mpmd_file'     # the shell command used to start the MPI execution described in a configuration file "mpmd_file"
mpi_n_flag = '-n '                                                          # the mpirun flag for specifying the number of tasks.
bash_get_rank = 'my_id=${SLURM_PROCID}'                                     # a bash expression that saves the MPI rank of this thread in the variable "my_id"
python_get_rank = 'my_id = int(os.environ["SLURM_PROCID"])'                 # a python expression that saves the MPI rank of this thread in the variable "my_id"

use_mpi_machinefile = ""

def machinefile_line(node, ntasks):
    return str(node)+':'+str(ntasks)

def get_node_list():
    # get SLURM's node list, can be in format bcn1001 or l[1009,1011,1013] or l[1009-1011,1013]
    import os
    nodes = os.environ["SLURM_NODELIST"]

    # just a single node -> there is no "[" in the srting
    if "[" not in nodes:
        return [nodes]

    # get machine name -> always is "l" for levante
    machine = nodes[0:1]

    # get list of comma separated values (cut out machine name and last "]")
    nodes = [node for node in nodes[len(machine)+1:-1].split(",")]

    #list of individual nodes
    node_list = []

    # go through list of comma separated values
    for node in nodes:
        # if theres is no minus this ellemtn is a indivual node
        if "-" not in node:
            node_list.append(machine+node)
            continue

        # range of nodes
        min_max = node.split("-")
        for node in range(int(min_max[0]),int(min_max[1])+1):
            node_list.append(machine+str(node))

    return node_list

# command to resubmit new job when final date is not yet reached, is executed in scripts/run
resubmit_command = "sbatch jobscript"
