import importlib.util
import os
import sys
import cftime
import datetime
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import matplotlib.pyplot as plt
import xarray as xr

try:
    tunelib_spec = importlib.util.spec_from_file_location("tunelib", "../../libraries/tunelib/__init__.py")
    tunelib = importlib.util.module_from_spec(tunelib_spec)
    sys.modules["tunelib"] = tunelib
    tunelib_spec.loader.exec_module(tunelib)
except FileNotFoundError:
    raise Exception('Could not find the tuning library, "tunelib", check file paths and available libraries.')


##
#  PREPARATIONS! set the stage for the tuning (init)


os.chdir("/projects/extern/nhr/nhr_mv/mvk00050/dir.lustre-emmy-hdd/IOW_ESM_MOM6_GOTM_testing")

configuration = {
    "general" : {
        "input_dir" : "./input/uncoupled_MOM6_k_ubc",
        "output_dir" : "./output/uncoupled_MOM6_k_ubc"
    }
}

configuration["parameters"] = {
    "cw" : {
        "file" : f"{configuration["general"]["input_dir"]}/MOM6_Baltic/INPUT/gotmturb.nml",
        "init" : 100.0,
        "range" : [50.0, 500.0],
        # a customized pattern compatible to re.search can be provided, 
        # the patter should contain two groups: 1st for the name and the assignment symbol (usually "="), 2nd for the numerical value of the parameter
        # default is 
        # "pattern" : rf"(^\s*{<parameter name>}\s*=\s*)(-?\d+(\.\d+)?)" 
        # which is some thing like " <parameter name>= x.yz ""
    },
    "alpha_Charnock" : {
        "file" : f"{configuration["general"]["input_dir"]}/MOM6_Baltic/input.nml",
        "init" : 1400.0,
        "range" : [100.0, 300000.0]
    },      
    "galp" : {
        "file" : f"{configuration["general"]["input_dir"]}/MOM6_Baltic/INPUT/gotmturb.nml",
        "init" : 0.53,
        "range" : [0.1, 1.0]
    },              
}

configuration["method"] = (tunelib.tuningmethods.NthOrderPolynomial, {"N" : 2})

stations = { 
    "BY5" : {"lat" : "55.25", "lon" : "15.98"},
    "F9" : {"lat" : "64.71", "lon" : "22.07"},
    "SR5" : {"lat" : "61.08", "lon" : "19.58"},
    "BY31" : {"lat" : "58.58", "lon" : "18.23"},
    "BY15" : {"lat" : "57.3333", "lon" : "20.05"},
    "LL7" : {"lat" : "59.8465", "lon" : "24.8378"},
}

lons = xr.DataArray([v["lon"] for k,v in stations.items()], dims=['station']) 
lats = xr.DataArray([v["lat"] for k,v in stations.items()], dims=['station']) 

stations = list(stations.keys())

begin_time = (2013,1,1)
end_time = (2017,3,31)

ref3d_to_sigma = [
    (xr.DataArray.sel, {"time" : slice(datetime.datetime(*begin_time), datetime.datetime(*end_time))}),
    (xr.DataArray.sel, {"longitude" : lons, "latitude" : lats, "method" : "nearest"}),
    (xr.DataArray.assign_coords, {"station" : stations}),
    (xr.DataArray.resample, {"time" : '1M'}),
    (xr.DataArray.mean, {"dim" :"time"}),
    (xr.DataArray.convert_calendar, {"calendar" : "standard"}),
    (xr.DataArray.interp, lambda model : {"depth" : model["z_l"].data}),
    (xr.DataArray.rename, {"depth" : "z_l"}) 
]

model3d_to_sigma = [
    (xr.DataArray.sel, {"time" : slice(cftime.DatetimeJulian(*begin_time, has_year_zero=False), cftime.DatetimeJulian(*end_time, has_year_zero=False))}),
    (xr.DataArray.sel, {"xh" : lons, "yh" : lats, "method" : "nearest"}),
    (xr.DataArray.assign_coords, {"station" : stations}),
    (xr.DataArray.resample, {"time" : '1M'}),
    (xr.DataArray.mean, {"dim" :"time"}),
    (xr.DataArray.convert_calendar, {"calendar" : "standard"})
]

root_mean_square_error = [
    (lambda x : x**2, {}),
    (xr.DataArray.mean, {"dim" : "time"}),
    (xr.DataArray.mean, {"dim" : ["z_l", "station"]}),
    (lambda x : x**0.5, {})    
] 


configuration["metric"] = {
    "temperature" : {
        "ref" : {"source" : "../reanalysis/CMEMS/cmems_mod_bal_phy_my_P1M-m.nc", "varname" : "thetao", "kw_args" : {"chunks" : {"longitude" : 10, "latitude" : 10, "time" : 10}}},
        "ref_mapping" : ref3d_to_sigma, 

        "model" : {"source" : f"{configuration["general"]["output_dir"]}/MOM6_Baltic/*/out_raw/*.ocean_3d_z.nc", "varname" : "temp", "kw_args" : {"chunks" : {"xh" : 10, "yh" : 10, "time" : 10}}},
        "model_mapping" : model3d_to_sigma,

        "contraction" : root_mean_square_error
    },
    "salinity" : {
        "ref" : {"source" : "../reanalysis/CMEMS/cmems_mod_bal_phy_my_P1M-m.nc", "varname" : "so", "kw_args" : {"chunks" : {"longitude" : 10, "latitude" : 10, "time" : 10}}},
        "ref_mapping" : ref3d_to_sigma, 

        "model" : {"source" : f"{configuration["general"]["output_dir"]}/MOM6_Baltic/*/out_raw/*.ocean_3d_z.nc", "varname" : "salt", "kw_args" : {"chunks" : {"xh" : 10, "yh" : 10, "time" : 10}}},
        "model_mapping" : model3d_to_sigma,

        "contraction" : root_mean_square_error
    },
}

tuning = tunelib.ensemblehandler.EnsembleHandler(configuration)

##
#  move this to create_job, if ensembleHandler exists then
tuning.generate_parameter_set()

##
#  show generated parameter set

# print(tuning.get_parameter_table())

# for name, value in tuning.get_parameter_set().items():
#     if "1" in list(name)[2:] or "2" in list(name)[2:]:
#         continue
#     plt.scatter(*value[[0,1]])
#     plt.text(*value[[0,1]], name)
# plt.xlim(tuning.method.configuration["cw"]["range"])
# plt.ylim(tuning.method.configuration["alpha_Charnock"]["range"])
# plt.xlabel("cw")
# plt.ylabel("alpha_Charnock")
# plt.grid()
# plt.show()

tuning.distribute_parameter_set()

##
#  ACTION! start tuning -> needs to be moved to post production
# FIXME: RUN at this point, everything below assumes the ensemble is available

tuning.evaluate()

# forward evaluation of the surrogate model
pstar = tuning.forward()

parameters = list(tuning.parameter_set.keys())
print(parameters)
for p in parameters:

    label1 = label2 = label3 = None
    if p == parameters[-1]:
        label1 = r"temperature"
        label2 = r"actual $\epsilon = $ temperature+salinity"
        label3 = r"estimated $\epsilon$"

    print(p, tuning.metric[p].epsilon["salinity"], tuning.metric[p].epsilon["temperature"])

    #plt.scatter(p, tuning.metric[p].epsilon["temperature"], color="green", marker="x", s=100, label=label1)
    plt.scatter(p, tuning.metric[p].epsilon["salinity"]+tuning.metric[p].epsilon["temperature"], color="red", marker="x", s=100, label=label2)
    plt.scatter(p, tuning.get_epsilon_estimate(tuning.get_parameter_set()[p]), color="black", marker="s", facecolor='none', s=100,  label=label3)

plt.scatter("*", tuning.get_epsilon_estimate(pstar), color="blue", marker="*", s=200, label=r"estimated minimum")    
plt.xlabel(r"$\vec{p}$")
plt.ylabel(r"$\epsilon(\vec{p})$")
plt.grid(linestyle="--", color="grey")
plt.xticks(rotation=45)
plt.legend()
plt.show()

print(r"p* = ", pstar)