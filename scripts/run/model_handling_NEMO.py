# This script will fill the required files into the work directory for a MOM6 model.
# The function is called from create_work_directories.py 

from datetime import datetime
import os
import shutil

import change_in_namelist
import get_run_information

import glob

from netCDF4 import Dataset
import numpy as np

import re

import model_handling

class ModelHandler(model_handling.ModelHandlerBase):
    def __init__(self, global_settings, my_directory):
        # initialize base class 
        model_handling.ModelHandlerBase.__init__(self, 
                                                 model_handling.ModelTypes.bottom,  # specify this model as a bottom model
                                                 global_settings,                   # pass global settings 
                                                 my_directory,                      # memorize specific directory name = "model_domain"
                                                 grids = [model_handling.GridTypes.t_grid, model_handling.GridTypes.u_grid, model_handling.GridTypes.v_grid]) # model grids
        
    def create_work_directory(self, work_directory_root, start_date, end_date):
    
        # STEP 0: get local parameters from global settings
        IOW_ESM_ROOT        = self.global_settings.root_dir              # root directory of IOW ESM
        init_date           = self.global_settings.init_date             # 'YYYYMMDD'
        coupling_time_step  = self.global_settings.coupling_time_step    # in seconds
        run_name            = self.global_settings.run_name              # string
        debug_mode          = self.global_settings.debug_mode            # FALSE if executables compiled for production mode shall be used, 
                                                            # TRUE if executables compiled for debug mode shall be used
                                                            
        my_directory        = self.my_directory             # name of model's input folder
        
        # STEP 1: CHECK IF EXECUTABLE ALREADY EXISTS, IF NOT COPY IT
        full_directory = work_directory_root+'/'+my_directory
        destfile = full_directory+'/nemo.exe'
        if not os.path.isfile(destfile):
            # no executable, need to copy
            if debug_mode:
                sourcefile = IOW_ESM_ROOT+'/components/NEMO/cfgs/NEMO_BALTIC_SEA9/BLD/bin/nemo.exe'
            else:
                sourcefile = IOW_ESM_ROOT+'/components/NEMO/cfgs/NEMO_BALTIC_SEA9/BLD/bin/nemo.exe'
            # check if file exists
            if os.path.isfile(sourcefile):
                shutil.copyfile(sourcefile,destfile)   # copy the file
            else:
                print('ERROR creating work directory '+full_directory+': Wanted to copy the executable from '+sourcefile+
                      ' but that does not exist. You may have to build it.')
                
        st = os.stat(destfile)                 # get current permissions
        os.chmod(destfile, st.st_mode | 0o777) # add a+rwx permission                
                
        sourcefile = IOW_ESM_ROOT+'/libraries/XIOS/bin/xios_server.exe'
        destfile = full_directory+'/xios_server.exe'
        shutil.copyfile(sourcefile,destfile)

        st = os.stat(destfile)                 # get current permissions
        os.chmod(destfile, st.st_mode | 0o777) # add a+rwx permission

        # STEP 2: Adjust times in input.nml
        my_startdate = datetime.strptime(start_date,'%Y%m%d')
        my_enddate = datetime.strptime(end_date,'%Y%m%d')
        my_initdate = datetime.strptime(init_date,'%Y%m%d')


        with open(f"{full_directory}/namelist/namelist_ref", "r") as fp:
            namelist = fp.read()

        with open(f"{full_directory}/namelist/namelist_ice_ref", "r") as fp:
            namelist_ice = fp.read()  

        # Change rn_Dt 
        pattern = r"\brn_Dt\s*=\s*([0-9.]+)"
        modelDT = float(re.search(pattern, namelist)[0].split("=")[-1])
                             
        # I guess number_of_step needs to be an integer
        number_of_steps = int(((my_enddate - my_startdate).days * 3600 * 24) // modelDT)

        print(start_date, end_date, init_date)
        print(number_of_steps)

        change_in_namelist.change_in_namelist(filename=full_directory+'/namelist/namelist_ref',
                         after='&namrun', before='/', start_of_line='nn_itend',
                         new_value = '='+str(number_of_steps))
        change_in_namelist.change_in_namelist(filename=full_directory+'/namelist/namelist_ref',
                         after='&namrun', before='/', start_of_line='nn_date0',
                         new_value = '='+str(start_date))      

        change_in_namelist.change_in_namelist(filename=full_directory+'/namelist/namelist_ref',
                         after='&namrun', before='/', start_of_line='cn_exp',
                         new_value = f'="{run_name}"')   
        
        os.system(f"mv {full_directory}/namelist/* {full_directory}/")
        os.system(f"rm -r {full_directory}/namelist")

        os.system(f"mv {full_directory}/xml/* {full_directory}/")
        os.system(f"rm -r {full_directory}/xml")
        os.system(f"mv {full_directory}/bathy/* {full_directory}/")
        os.system(f"rm -r {full_directory}/bathy")

        # STEP 3: Create an empty folder named "RESTART"
        # but we add the flag that no error is returned if this already exists
        os.makedirs(f'{full_directory}/RESTART', exist_ok=True) 

        # # STEP 4: Copy hotstart files if a corresponding folder exists
        # TODO I think we need to adapth how the hotstart is handled.
        if (start_date != init_date):
            hotstart_folder = IOW_ESM_ROOT + '/hotstart/' + run_name + '/' + my_directory + '/' + start_date
            os.system(f"cp {hotstart_folder}/* {full_directory}/RESTART/.")
        # otherwise use initial data in init folder for a cold start
        else:
            if not os.path.isdir(full_directory + '/init'):
                print("ERROR: For a cold start an INIT folder must be provided but could not be found. The model will probably crash.")
            else:
                os.system('cp '+full_directory+'/init/* '+full_directory+'/RESTART/')

        return
        
    def check_for_success(self, work_directory_root, start_date, end_date):

        # if NEMO has succeeded there is a RESTART folder
        hotstartfile = work_directory_root+'/'+self.my_directory+'/RESTART/*_ocean_restart.nc'
        
        # if restart folder is empty we failed
        if glob.glob(hotstartfile) == []:
            print('run failed because no file exists:' + hotstartfile)
            return False
        # we succeeded

        return True
    
    def move_results(self, work_directory_root, start_date, end_date):

        run_name = self.global_settings.run_name
    
        # work directory of this model instance
        workdir = work_directory_root + '/' + self.my_directory
        # directory for output        
        outputdir = self.global_settings.root_dir + '/output/' + self.global_settings.run_name+'/'+self.my_directory+'/'+str(start_date)
        # directory for hotstarts
        hotstartdir = self.global_settings.root_dir + '/hotstart/' + self.global_settings.run_name+'/'+self.my_directory+'/'+str(end_date)   

        # STEP 1: CREATE DIRECTORIES IF REQUIRED
        if (not os.path.isdir(outputdir)): 
            os.makedirs(outputdir)
        if (not os.path.isdir(hotstartdir)):
            os.makedirs(hotstartdir)

        # STEP 2: MOVE OUTPUT
        # TODO this likely copies additional wrong files
        os.system(f'mv {workdir}/{run_name}*.nc {outputdir}/.')

        # TODO remove later
        # Here we could add ocean_output
        # os.system('mv '+workdir+'/logfile_*.txt '+outputdir+'/.')

        # if we run with verbos flux calculator the exchanged fields are stored in files MS*.nc and MR*.nc
        # if files are present we keep them
        if glob.glob(workdir+'/NS*.nc') != []:
            os.system('mv '+workdir+'/MS*.nc '+outputdir+'/.')
        if glob.glob(workdir+'/NR*.nc') != []:
            os.system('mv '+workdir+'/MR*.nc '+outputdir+'/.')
        
        # store run information (commit ID's of built components, global_settings,...)
        if os.path.isfile(workdir + '/RUN_INFO'):     
            os.system('mv '+workdir+'/RUN_INFO '+outputdir+'/.')

        # keep the important input files
        files_to_keep = ["namelist_ref.nml", "namelist_ice_ref.nml", "namelist_cfg", "field_def_nemo-oce.xml", "field_def_nemo-ice.xml"]
        for file in files_to_keep:
            os.system('mv '+workdir+'/'+file+' '+outputdir+'/.')

        # STEP 3: MOVE HOTSTART
        os.system('mv '+workdir+'/RESTART/*_ocean_restart.nc '+hotstartdir+'/ocean_restart.nc')  # MOM hotstart files
        os.system('mv '+workdir+'/RESTART/*_ice_restart.nc '+hotstartdir+'/ice_restart.nc')  # MOM hotstart files
    
    def grid_convert_to_SCRIP(self):
        my_directory = self.my_directory       # name of this model instance
    
        # STEP 1: CREATE EMPTY "mappings" SUBDIRECTORY
        full_directory = self.global_settings.input_dir+'/'+my_directory
        if (os.path.isdir(full_directory+'/mappings')):
            os.system('rm -r '+full_directory+'/mappings')
        os.system('mkdir '+full_directory+'/mappings')

        # STEP 3: CONVERT THE GRIDS TO SCRIP FORMAT
        t_grid_file = full_directory+'/mappings/t_grid.nc'
        u_grid_file = full_directory+'/mappings/u_grid.nc'
        v_grid_file = full_directory+'/mappings/v_grid.nc'   # in case of MOM5, both u and v grid are actually the c-grid
        t_grid_title = my_directory+'_t_grid'
        u_grid_title = my_directory+'_u_grid'
        v_grid_title = my_directory+'_v_grid'


        inputfile = full_directory+'/INPUT/ocean_hgrid.nc'

        # STEP 2: CHECK IF grid_spec.nc EXISTS
        if not (os.path.isfile(inputfile)):
            print('ERROR in model_handling_MOM6.grid_convert_to_SCRIP: File '+inputfile+' not found.')
            return       

        # maximal number of vertices of grid cells
        n_grid_corners = 4

        # open dataset
        nc = Dataset(inputfile,"r")
        # get "super-grid" information
        x = nc.variables['x'][:,:]
        y = nc.variables['y'][:,:]
        nc.close()

        maskfile = full_directory+'/INPUT/ocean_mask.nc'

        # STEP 2: CHECK IF grid_spec.nc EXISTS
        if not (os.path.isfile(maskfile)):
            print('ERROR in model_handling_MOM6.grid_convert_to_SCRIP: File '+maskfile+' not found.')
            return        

        # open dataset
        nc = Dataset(maskfile,"r")
        # get info whether cells are active
        mask_t = nc.variables['mask'][:,:]
        nc.close()

        # centers of actual grid are stored in every second entry of the super grid (see https://gist.github.com/adcroft/c1e207024fe1189b43dddc5f1fe7dd6c)
        x_t_center = x[1::2,1::2]
        y_t_center = y[1::2,1::2]

        x_t_vert = np.full((*x_t_center.shape, n_grid_corners), np.nan)
        y_t_vert = np.full((*y_t_center.shape, n_grid_corners), np.nan)

        # south-west corner
        x_t_vert[:,:,0] = x[:-1:2,:-1:2]
        y_t_vert[:,:,0] = y[:-1:2,:-1:2]
        # south-east corner
        x_t_vert[:,:,1] = x[0:-1:2,2::2]
        y_t_vert[:,:,1] = y[0:-1:2,2::2]
        # north-east corner
        x_t_vert[:,:,2] = x[2::2,2::2]
        y_t_vert[:,:,2] = y[2::2,2::2]
        # north-west corner
        x_t_vert[:,:,3] = x[2::2,:-1:2]
        y_t_vert[:,:,3] = y[2::2,:-1:2]


        # write netCDF file
        # get index dimensions
        grid_dims = x_t_center.shape
        n_grid_cells = np.prod(grid_dims)
        n_grid_rank = len(x_t_center.shape)

        # enumerate indices
        grid_cell_index = np.arange(n_grid_cells)
        grid_corners = np.arange(n_grid_corners)
        grid_rank    = np.arange(n_grid_rank)

        # get values for t grid
        # centers
        grid_center_lon = x_t_center.flatten()
        grid_center_lat = y_t_center.flatten()

        # corners (vertices)
        grid_corner_lon = x_t_vert.reshape(n_grid_cells, n_grid_corners)
        grid_corner_lat = y_t_vert.reshape(n_grid_cells, n_grid_corners)

        # mask
        grid_imask      = mask_t.flatten().astype(int)

        # delete t-grid file if it exists
        if os.path.isfile(t_grid_file):
            os.remove(t_grid_file)
        # write t grid file
        nc = Dataset(t_grid_file,"w")
        nc.title = t_grid_title
        nc.createDimension("grid_size"   ,n_grid_cells  ); grid_size_var    = nc.createVariable("grid_size"   ,"i4",("grid_size"   ,)); grid_size_var[:]    = grid_cell_index
        nc.createDimension("grid_corners",n_grid_corners); grid_corners_var = nc.createVariable("grid_corners","i4",("grid_corners",)); grid_corners_var[:] = grid_corners
        nc.createDimension("grid_rank"   ,n_grid_rank   ); grid_rank_var    = nc.createVariable("grid_rank"   ,"i4",("grid_rank"   ,)); grid_rank_var[:]    = grid_rank
        grid_dims_var       = nc.createVariable("grid_dims"      ,"i4",("grid_rank",               )); grid_dims_var.missval=np.int32(-1) ; grid_dims_var[:]      =[grid_dims[1],grid_dims[0]]
        grid_center_lat_var = nc.createVariable("grid_center_lat","f8",("grid_size",               )); grid_center_lat_var.units="degrees"; grid_center_lat_var[:]=grid_center_lat
        grid_center_lon_var = nc.createVariable("grid_center_lon","f8",("grid_size",               )); grid_center_lon_var.units="degrees"; grid_center_lon_var[:]=grid_center_lon
        grid_imask_var      = nc.createVariable("grid_imask"     ,"i4",("grid_size",               )); grid_imask_var.missval=np.int32(-1); grid_imask_var[:]     =grid_imask
        grid_corner_lat_var = nc.createVariable("grid_corner_lat","f8",("grid_size","grid_corners",)); grid_corner_lat_var.units="degrees"; grid_corner_lat_var[:]=grid_corner_lat
        grid_corner_lon_var = nc.createVariable("grid_corner_lon","f8",("grid_size","grid_corners",)); grid_corner_lon_var.units="degrees"; grid_corner_lon_var[:]=grid_corner_lon
        nc.sync()
        nc.close()


        x_u_center = x[1::2,::2]
        y_u_center = y[1::2,::2]

        x_u_vert = np.full((*x_u_center.shape, n_grid_corners), np.nan)
        y_u_vert = np.full((*y_u_center.shape, n_grid_corners), np.nan)

        # south-west corner
        x_u_vert[:,1:,0] = x[:-1:2,1::2]
        y_u_vert[:,1:,0] = y[:-1:2,1::2]
        x_u_vert[:,0,0] = x[:-1:2,0]
        y_u_vert[:,0,0] = y[:-1:2,0]
        # south-east corner
        x_u_vert[:,:-1,1] = x[:-1:2,1::2]
        y_u_vert[:,:-1,1] = y[:-1:2,1::2]
        x_u_vert[:,-1,1] = x[:-1:2,-1]
        y_u_vert[:,-1,1] = y[:-1:2,-1]
        # north-east corner
        x_u_vert[:,:-1,2] = x[2::2,1::2]
        y_u_vert[:,:-1,2] = y[2::2,1::2]
        x_u_vert[:,-1,2] = x[2::2,-1]
        y_u_vert[:,-1,2] = y[2::2,-1]
        # north-west corner
        x_u_vert[:,1:,3] = x[2::2,1::2]
        y_u_vert[:,1:,3] = y[2::2,1::2]
        x_u_vert[:,0,3] = x[2::2,0]
        y_u_vert[:,0,3] = y[2::2,0]

        x_u_center = x_u_center[:,1:]
        y_u_center = y_u_center[:,1:]
        x_u_vert = x_u_vert[:,1:,:]
        y_u_vert = y_u_vert[:,1:,:]

        # get mask for u grid from t-grid mask
        mask_u = np.c_[mask_t[:,1:]*mask_t[:,:-1], 0.0*mask_t[:,-1]]

        # write netCDF file
        # get index dimensions
        grid_dims = x_u_center.shape
        #print(grid_dims, x_u_vert.shape, y_u_vert.shape)
        n_grid_cells = np.prod(grid_dims)
        n_grid_rank = len(x_u_center.shape)

        # enumerate indices
        grid_cell_index = np.arange(n_grid_cells)
        grid_corners = np.arange(n_grid_corners)
        grid_rank    = np.arange(n_grid_rank)        

        # get values for c grid
        grid_center_lon = x_u_center.flatten()
        grid_center_lat = y_u_center.flatten()
        grid_imask      = mask_u.flatten().astype(int)

        grid_corner_lon = np.reshape(x_u_vert, (n_grid_cells, n_grid_corners))
        grid_corner_lat = np.reshape(y_u_vert, (n_grid_cells, n_grid_corners))
        
        # delete u-grid file if it exists
        if os.path.isfile(u_grid_file):
            os.remove(u_grid_file)
        # write u grid file
        nc = Dataset(u_grid_file,"w")
        nc.title = u_grid_title
        nc.createDimension("grid_size"   ,n_grid_cells  ); grid_size_var    = nc.createVariable("grid_size"   ,"i4",("grid_size"   ,)); grid_size_var[:]    = grid_cell_index
        nc.createDimension("grid_corners",n_grid_corners); grid_corners_var = nc.createVariable("grid_corners","i4",("grid_corners",)); grid_corners_var[:] = grid_corners
        nc.createDimension("grid_rank"   ,n_grid_rank   ); grid_rank_var    = nc.createVariable("grid_rank"   ,"i4",("grid_rank"   ,)); grid_rank_var[:]    = grid_rank
        grid_dims_var       = nc.createVariable("grid_dims"      ,"i4",("grid_rank",               )); grid_dims_var.missval=np.int32(-1) ; grid_dims_var[:]      =[grid_dims[1],grid_dims[0]]
        grid_center_lat_var = nc.createVariable("grid_center_lat","f8",("grid_size",               )); grid_center_lat_var.units="degrees"; grid_center_lat_var[:]=grid_center_lat
        grid_center_lon_var = nc.createVariable("grid_center_lon","f8",("grid_size",               )); grid_center_lon_var.units="degrees"; grid_center_lon_var[:]=grid_center_lon
        grid_imask_var      = nc.createVariable("grid_imask"     ,"i4",("grid_size",               )); grid_imask_var.missval=np.int32(-1); grid_imask_var[:]     =grid_imask
        grid_corner_lat_var = nc.createVariable("grid_corner_lat","f8",("grid_size","grid_corners",)); grid_corner_lat_var.units="degrees"; grid_corner_lat_var[:]=grid_corner_lat
        grid_corner_lon_var = nc.createVariable("grid_corner_lon","f8",("grid_size","grid_corners",)); grid_corner_lon_var.units="degrees"; grid_corner_lon_var[:]=grid_corner_lon
        nc.sync()
        nc.close()        


        mask_v = np.r_[mask_t[1:,:]*mask_t[:-1,:], [0.0*mask_t[-1,:]]]

        x_v_center = x[::2,1::2]
        y_v_center = y[::2,1::2]

        x_v_vert = np.full((*x_v_center.shape, n_grid_corners), np.nan)
        y_v_vert = np.full((*y_v_center.shape, n_grid_corners), np.nan)

        # south-west corner
        x_v_vert[1:,:,0] = x[1::2,:-1:2]
        y_v_vert[1:,:,0] = y[1::2,:-1:2]
        x_v_vert[0,:,0] = x[0,:-1:2]
        y_v_vert[0,:,0] = y[0,:-1:2]
        # south-east corner
        x_v_vert[1:,:,1] = x[1::2,2::2]
        y_v_vert[1:,:,1] = y[1::2,2::2]
        x_v_vert[0,:,1] = x[0,2::2]
        y_v_vert[0,:,1] = y[0,2::2]
        # north-east corner
        x_v_vert[:-1,:,2] = x[1::2,2::2]
        y_v_vert[:-1,:,2] = y[1::2,2::2]
        x_v_vert[-1,:,2] = x[-1,2::2]
        y_v_vert[-1,:,2] = y[-1,2::2]
        # north-west corner
        x_v_vert[:-1,:,3] = x[1::2,:-1:2]
        y_v_vert[:-1,:,3] = y[1::2,:-1:2]
        x_v_vert[-1,:,3] = x[-1,:-1:2]
        y_v_vert[-1,:,3] = y[-1,:-1:2]

        x_v_center = x_v_center[1:,:]
        y_v_center = y_v_center[1:,:]
        x_v_vert = x_v_vert[1:,:,:]
        y_v_vert = y_v_vert[1:,:,:]


        # write netCDF file
        # get index dimensions
        grid_dims = x_v_center.shape
        n_grid_cells = np.prod(grid_dims)
        n_grid_rank = len(x_v_center.shape)

        # enumerate indices
        grid_cell_index = np.arange(n_grid_cells)
        grid_corners = np.arange(n_grid_corners)
        grid_rank    = np.arange(n_grid_rank)        

        # get values for c grid
        grid_center_lon = x_v_center.flatten()
        grid_center_lat = y_v_center.flatten()
        grid_imask      = mask_v.flatten().astype(int)

        grid_corner_lon = np.reshape(x_v_vert, (n_grid_cells, n_grid_corners))
        grid_corner_lat = np.reshape(y_v_vert, (n_grid_cells, n_grid_corners))


        # delete v-grid file if it exists
        if os.path.isfile(v_grid_file):
            os.remove(v_grid_file)
        # write v grid file
        nc = Dataset(v_grid_file,"w")
        nc.title = v_grid_title
        nc.createDimension("grid_size"   ,n_grid_cells  ); grid_size_var    = nc.createVariable("grid_size"   ,"i4",("grid_size"   ,)); grid_size_var[:]    = grid_cell_index
        nc.createDimension("grid_corners",n_grid_corners); grid_corners_var = nc.createVariable("grid_corners","i4",("grid_corners",)); grid_corners_var[:] = grid_corners
        nc.createDimension("grid_rank"   ,n_grid_rank   ); grid_rank_var    = nc.createVariable("grid_rank"   ,"i4",("grid_rank"   ,)); grid_rank_var[:]    = grid_rank
        grid_dims_var       = nc.createVariable("grid_dims"      ,"i4",("grid_rank",               )); grid_dims_var.missval=np.int32(-1) ; grid_dims_var[:]      =grid_dims
        grid_center_lat_var = nc.createVariable("grid_center_lat","f8",("grid_size",               )); grid_center_lat_var.units="degrees"; grid_center_lat_var[:]=grid_center_lat
        grid_center_lon_var = nc.createVariable("grid_center_lon","f8",("grid_size",               )); grid_center_lon_var.units="degrees"; grid_center_lon_var[:]=grid_center_lon
        grid_imask_var      = nc.createVariable("grid_imask"     ,"i4",("grid_size",               )); grid_imask_var.missval=np.int32(-1); grid_imask_var[:]     =grid_imask
        grid_corner_lat_var = nc.createVariable("grid_corner_lat","f8",("grid_size","grid_corners",)); grid_corner_lat_var.units="degrees"; grid_corner_lat_var[:]=grid_corner_lat
        grid_corner_lon_var = nc.createVariable("grid_corner_lon","f8",("grid_size","grid_corners",)); grid_corner_lon_var.units="degrees"; grid_corner_lon_var[:]=grid_corner_lon
        nc.sync()
        nc.close()
        
    def get_model_executable(self):
        return "nemo.exe"
        
    def get_num_threads(self):
        # MOM6 model - parallelization is given in input.nml in section &ocean_model_nml
        # "layout = 14,10"  e.g. means 14x10 rectangles exist, but a few of them may be masked out
        # "mask_table = 'INPUT/mask_table'" (optional) means we will find this file there
        # it contains the number of masked (=inactive) rectangles in the first line
        model               = self.my_directory             # name of model's input folder
        
        inputfile = self.global_settings.input_dir+'/'+model+'/namelist/namelist_ref'

        if not os.path.isfile(inputfile):
            print('Could not determine parallelization layout because the following file was missing: '+inputfile)
            return 0
        
        with open(inputfile) as f:
            for line in f:
                if re.search(r"^\s*!\s*NPROC\s*=", line):
                    return int(line.replace(" ","").split("!")[-1].split("=")[-1])
                
        return 0

    def get_domain_decomposition(self):

        # get the correct paths
        full_directory = self.global_settings.input_dir+'/' + self.my_directory


        inputfile = full_directory+'/INPUT/ocean_hgrid.nc'

        # STEP 2: CHECK IF grid_spec.nc EXISTS
        if not (os.path.isfile(inputfile)):
            print('ERROR in model_handling_MOM6.get_domain_decomposition: File '+inputfile+' not found.')
            return       

        # open dataset
        nc = Dataset(inputfile,"r")
        # get "super-grid" information
        x = nc.variables['x'][:,:]
        nc.close()

        x_t_center = x[1::2,1::2]
        grid_dims = x_t_center.shape


        # get the mask file (use get_num_threads method that sets self.maskfile, self.ndivx, self.ndivy)
        if self.get_num_threads() <= 0:
            print("Getting the number of threads failed. Abort.")
            return []

        # mimic domain decomposition from MOM5 code (mpp_domain.c)
        def mpp_compute_extent(npts, ndivs):

            #print(npts, ndivs)
            
            ibegin= [0]*ndivs
            iend = [0]*ndivs

            ndivs_is_odd = ndivs%2
            npts_is_odd = npts%2
            symmetrize = 0

            if( ndivs_is_odd and npts_is_odd ):
                symmetrize = 1
            if( ndivs_is_odd == 0 and npts_is_odd == 0 ):
                symmetrize = 1
            if( ndivs_is_odd and npts_is_odd == 0 and ndivs < npts/2 ):
                symmetrize = 1

            isg = 0
            ieg = npts-1
            ist = isg

            for ndiv in range(0,ndivs):

                #mirror domains are stored in the list and retrieved if required. 
                if( ndiv == 0 ): # initialize max points and max domains 
                    imax = ieg
                    ndmax = ndivs
                # do bottom half of decomposition, going over the midpoint for odd ndivs
                if( ndiv < (ndivs-1)//2+1 ):
                    # domain is sized by dividing remaining points by remaining domains
                    ie = ist + np.ceil((imax-ist+1.0)/(ndmax-ndiv) ) - 1
                    ndmirror = (ndivs-1) - ndiv # mirror domain

                    if( (ndmirror > ndiv) and symmetrize ): # only for domains over the midpoint
	                    # mirror extents, the max(,) is to eliminate overlaps
                        ibegin[ndmirror] = max(isg+ieg-ie, ie+1)
                        iend[ndmirror] = max(isg+ieg-ist, ie+1)
                        imax = ibegin[ndmirror] - 1
                        ndmax -= 1
                else:
                    if( symmetrize ):
	                    #do top half of decomposition by retrieving saved values */
                        ist = ibegin[ndiv]
                        ie = iend[ndiv]
    
                    else:
                        ie = ist + np.ceil((imax-ist+1.0)/(ndmax-ndiv)) - 1

                ibegin[ndiv] = ist
                iend[ndiv] = ie

                ist = ie + 1              

            return ibegin, iend                        

        # get the index limits for the domains 
        ibeginx, iendx = mpp_compute_extent(grid_dims[1], self.ndivx)   # x direction
        ibeginy, iendy = mpp_compute_extent(grid_dims[0], self.ndivy)   # y direction

        print("Use domain extents in x direction as: ", np.array(iendx, dtype=int) - np.array(ibeginx, dtype=int) + 1)
        print("Use domain extents in y direction as: ", np.array(iendy, dtype=int) - np.array(ibeginy, dtype=int) + 1)

        # get mask file content
        with open(self.maskfile) as mf:
            lines = mf.readlines()

        # throw away first two lines
        lines.pop(0)    # number of masked domains
        lines.pop(0)    # layout

        # matrix containing the process indeces
        processes = np.zeros(shape=(self.ndivx, self.ndivy), dtype=int)

        # go through the lines and mark masked unused domains with -1
        for line in lines:
            i,j = line.strip().split(",")
            i = int(i)
            j = int(j)
            processes[i-1][j-1] = -1

        # count remaining processes (that are not masked out)
        counter = 0

        for j in range(0, self.ndivy):
            for i in range(0, self.ndivx):

                if processes[i][j] == -1:
                    continue
                
                processes[i][j] = counter
                counter += 1

        #print(processes)

        # build array with task index for each grid point
        tasks = []
        for j in range(0, grid_dims[0]):
            # check in which domain this gridpoint is in y direction
            for k,endy in enumerate(iendy):
                if (j <= endy) and (j >= ibeginy[k]):
                    ky = k
                    break
            for i in range(0, grid_dims[1]):
                # check in which domain this gridpoint is in x direction
                for k,endx in enumerate(iendx):
                    if (i <= endx) and (i >= ibeginx[k]):
                        kx = k
                        break

                # store the task corresponding to this grid point           
                tasks.append(processes[kx][ky]) 

        return tasks


