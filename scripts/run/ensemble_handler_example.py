import importlib.util
import sys
import os
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import matplotlib.pyplot as plt
import xarray as xr

try:
    tunelib_spec = importlib.util.spec_from_file_location('tunelib', '../../libraries/tunelib/__init__.py')
    tunelib = importlib.util.module_from_spec(tunelib_spec)
    sys.modules['tunelib'] = tunelib
    tunelib_spec.loader.exec_module(tunelib)
except FileNotFoundError:
    raise Exception('Could not find the tuning library, "tunelib", check file paths and available libraries.')


##
#  init ensemble configuration with parameter settings and tuning method

configuration = {
    'parameters': {
        'uc1': {
            'file': f"./CCLM_Eurocordex/INPUT_ORG",
            'init': 0.58,
            'range': [0.2, 0.8],
        },
        'qi0': {
            'file': f"./CCLM_Eurocordex/INPUT_ORG",
            'init': 0.0,
            'range': [0.0, 0.01],
        },
        'clc_diag': {
            'file': f"./CCLM_Eurocordex/INPUT_ORG",
            'init': 0.5,
            'range': [0.1, 0.9],
       },
    }
}

configuration['method'] = (tunelib.tuningmethods.NthOrderPolynomial, {'N' : 2})


##
#  unmodified inherited class from default EnsembleHandler with local configuration

class BellpratEnsemble(tunelib.ensemblehandler.EnsembleHandler):

    def __init__(self, global_settings) -> None:

        super().__init__(configuration, global_settings)

