# -*- coding: utf-8 -*-
"""
Created on Mon Dec 2 15:00:00 2024

@author: Georg Sebastian Voelker
"""

from PySide6.QtCore import Slot

# cannot be inherited, thus it is set as equivalent
IowEsmFrontendDecorator = Slot
