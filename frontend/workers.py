# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 09:30:00 2024

@author: Georg Sebastian Voelker
"""

from typing import Callable
from io import StringIO
import traceback, sys
from contextlib import contextmanager
import os
import signal

from PySide6.QtCore import (
    QRunnable, Signal, Slot, QObject
)


@contextmanager
def stdout_redirector(stream: StringIO) -> any:
    """Temporarily redirect stdout to an output stream. Implemented as context manager

    Args:
        stream (io.StringIO): output stream
    """
    old_stdout = sys.stdout
    sys.stdout = stream
    try:
        yield
    finally:
        sys.stdout = old_stdout

        
class WorkerSignals(QObject):
    """Class carrying the signals for the workers, inherits from 'PySide6.QtCore.QObject'.

    Args:
        QObject (QObject): parent class: PySide6.QtCore.QObject
    """
    started = Signal()
    finished = Signal(str)
    error = Signal(tuple)
    result = Signal(str)
    stdout = Signal(object)
    
        
class WorkerRunner(QRunnable):
    """A QRunner subclass that creates a worker for the QThreadPool, inherits from QRunnable.

    Args:
        QRunnable (PySide6.QtCore.QRubnnable): PySide6 QRunnable class
    """

    def __init__(self, task: Callable, verbose: bool) -> None:
        """Instantiate a worker to run tasks in the background. Emits signaly to connect to and pipes received stdout to the stdout signal.

        Args:
            task (Callable): callable to be executed, needs to support polling and stdout piping as done by 'subprocess.Popen'
            verbose (bool): set to True if stdout should be piped through the stdout signal
        """
        super().__init__()
        
        self.task = task
        self.signals = WorkerSignals()
        self.io = StringIO()
        self._stopped = False
        self._verbose = verbose
        self.proc = None
    
    @Slot()
    def run(self) -> None:
        """Run the task.
        """
        self.signals.started.emit()
        try:
            with stdout_redirector(self.io):
                self.proc = self.task()
                
                while(self.proc.poll() is None):
                    line = self.proc.stdout.readline()
                    if self._verbose:
                        self.receive_stdout(line)
                    
                    if self._stopped:
                        self._kill()
            
            self.signals.finished.emit('success')
                        
        except AttributeError:
            self.signals.result.emit('Cannot poll the process, please provide a (partial) task of type "subprocess.Popen".')
            self.signals.finished.emit('error')
            
        except BaseException:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
            self.signals.finished.emit('error')
            
        finally:
            # always set stop flag after execution
            self._stopped = True
    
    def receive_stdout(self, message: str | bytes) -> None:
        """Receive message and send it through the stdout signal.

        Args:
            message (str): message to be sent
        """
        self.signals.stdout.emit(message)
    
    @Slot()
    def isRunning(self) -> bool:
        """Returns True if the worker is currently running, False otherwise.

        Returns:
            bool: worker status
        """
        if self._stopped:
            return False
        else:
            return True
            
    @Slot()
    def terminate(self) -> None:
        """Terminate the worker.
        """
        self._stopped = True
        
    @Slot()
    def _kill(self) -> None:
        """Internal procedure to kill the process and terminate the worker.
        """
        
        # terminate execution
        os.killpg(os.getpgid(self.proc.pid), signal.SIGTERM)
        self.signals.finished.emit('canceled')
        
        # disconnect all signals
        self.signals.error.disconnect()
        self.signals.started.disconnect()
        self.signals.result.disconnect()
        self.signals.finished.disconnect()
        self.signals.stdout.disconnect()
        


