from __future__ import annotations
from functools import partial
from typing import Callable

import frontend
import backend

from datetime import datetime
from time import sleep
import os
import glob
import re

from PySide6.QtCore import (
    Qt, Slot, Signal, QObject, QThreadPool
)

from PySide6.QtGui import (
    QDesktopServices, QFont, QPalette, QColor,
    QFontDatabase, QShortcut, QIcon
)
from PySide6.QtSvgWidgets import QSvgWidget

from PySide6.QtWidgets import (
    QApplication, QCheckBox, QComboBox, QDialog,
    QGridLayout, QHBoxLayout, QVBoxLayout, QLabel,
    QMenu, QStyleFactory, QTextEdit, QToolButton,
    QWidget, QGroupBox, QTableWidget, QTableWidgetItem,
    QHeaderView, QAbstractScrollArea, QSizePolicy
)

# define extended ansi_escape mechanism
ansi_escape = re.compile(br'(?:\x1B[@-Z\\-_=>]|[\x80-\x9A\x9C-\x9F]|[\s][\x08]|(?:\x1B\[|\x9B)[0-?]*[ -/]*[@-~])')

# define menu wich stays open / reopens after clicking a checkbox in the menu
class QPersistentMenu(QMenu):
    """Class decorator for persistent menu .

    Args:
        QMenu (PySide6.QtWidgets.QMenu): PySide6 QMenu widget class
    """
    def actionEvent(self, event):
        super().actionEvent(event)
        self.show()
        
def init_widget(wdgt, name):
    """Init a widget with a name and a tooltip showing the name.

    Args:
        wdgt (QWidget): The widget to initialize.
        name (str): The name to set for the widget.
    """
    wdgt.setObjectName(name)
    wdgt.setToolTip(name)


class MainSignals(QObject):
    """Qt signals for the IOW-ESM dialogue, inherits from QObject

    Args:
        QObject (PySide6.QtCore): PySide6 QObject class
    """
    mod_worker = Signal()       # worker added or terminated
    terminator = Signal()       # terminate worker
    
    
class IowEsmGui(QDialog):
    """Main dialogue for the IOW-ESM Qt control center, inherits from QDialog. Call from "iow-esm-qt.py".

    Args:
        QDialog (PySide6.QtWidgets.QDialog): PySide6 QDialog widget class
    """

    def __init__(self) -> None:
        """Initialize the UI for the IOW - ESM .
        """
        super().__init__()
        
        # attach signal for dynamic display for number of workers
        self.signals = MainSignals()
        self.signals.mod_worker.connect(self.update_num_workers)
        
        # set theme to Fusion
        QApplication.setStyle(QStyleFactory.create('Fusion'))

        # init with dark(er) theme
        self.dark = True
        QApplication.setPalette(QPalette(QColor(backend.globals.IowColors.blue1)))
        
        # add window icon
        self.icon = QIcon()
        icon_path = os.path.join('backend', 'assets', 'logo_iow-esm.png')
        self.icon.addFile(icon_path)
        self.setWindowIcon(self.icon)
        
        # set menu / wide button width
        self.layout = {}
        self.layout['wide_button'] = 250
        self.layout['min_button'] = 125
        self.layout['width'] = 1200
        self.layout['height'] = 780

        # general layout definitions (and make them object properties)
        self.title_font = QFont('Arial', 24, QFont.Weight.Bold)
        self.subtitle_font = QFont('Arial', 14, QFont.Weight.Bold)
        self.setWindowTitle("IOW-ESM Cockpit")
        self.setGeometry(100, 100, self.layout['width'], self.layout['height'])
        
        # create element dictionaries
        self.buttons = {}               # all buttons (including menu buttons)
        self.dropdowns = {}             # simple dropdown buttons
        self.current = {}               # current chosen configs (for instance in dropdowns)
        self.menus = {}                 # menus with checkboxes
        self.subtitles = {}             # section titles        
        
        # init currents keywords
        for key in ['destination', 'build mode', 'rebuild mode', 'build_config', 'sync destination']:
            self.current[key] = ''
        
        # init currents lists
        for key in ['targets', 'inputs']:
            self.current[key] = []
        
        # init currents dictionaries
        for key in ['setups', 'origins']:
            self.current[key] = {}
        
        # build log first so builders can send logs.
        self._build_block_log()
        
        # get error handler and interface library with self as parent
        self.eh = backend.error_handler.IowEsmErrorHandler(self)
        self.intfc = backend.interfaces.IowEsmInterfaces(self.eh)
        
        # get destination and ensemble config dictionaries
        self.destinations = backend.auxclasses.FormatText(
            path='./DESTINATIONS',
            name='destinations config',
            parent=self,
            post_edit=self.update_destinations
        )
        self.ensemble_config = backend.auxclasses.FormatTable(
            path='./PPEconf.csv',
            name='ensemble config',
            parent=self,
            post_edit=None
        )
        self.setups = backend.auxclasses.FormatText(
            path='./SETUPS',
            name='setups config',
            parent=self,
            post_edit=self.update_available_setups
        )
        
        self.origins = backend.auxclasses.FormatText(
            path='./ORIGINS',
            name='origins',
            parent=self,
            post_edit=self.update_available_targets
        )
        
        # create lists of setups
        self.available_setups = list(self.setups.keys())    # setups available in SETUP file
        self.deployed_inputs = []                           # setups available at / already deployed to current destination
        
        # build layouts
        self._build_block_header()
        self._build_block_destinations()
        self._build_block_builder()
        self._build_block_setups()
        self._build_block_run()
        self._build_block_post()
        self._build_block_footer()

        # setup main layout and add all sublayouts
        self.main_layout = QGridLayout(self)
        self.main_layout.addWidget(self.header_widget, 0, 0, 1, 2)
        self.main_layout.addWidget(self.destination_widget, 1, 0, 1, 1)
        self.main_layout.addWidget(self.build_widget, 2, 0, 1, 1)
        self.main_layout.addWidget(self.setups_widget, 3, 0, 1, 1)
        self.main_layout.addWidget(self.run_widget, 4, 0, 1, 1)
        self.main_layout.addWidget(self.post_widget, 5, 0, 1, 1)
        self.main_layout.addWidget(self.log_widget, 1, 1, 6, 1)
        self.main_layout.addWidget(self.footer_widget, 7, 0, 1, 2)
        
        # set shortcuts
        self.shortcuts = {
            'override':          ('Esc',         self, self._dummy),
            'Documentation':     ('F1',          self, self.open_documentation),
            'Quit':              ('Ctrl+Q',      self, self.close),
            'Clone':             ('Alt+C',       self, self.clone_targets),
            'Build':             ('Alt+B',       self, self.build_targets),
            'Deploy':            ('Alt+D',       self, self.deploy_setups),
            'Run':               ('Alt+R',       self, self.start_runs),
            'Sync':              ('Alt+Y',       self, self.sync),
            'Toggle view':       ('Alt+V',       self, self.toggle_display_mode),
            'Help':              ('Alt+H',       self, self.open_shortcuts),
            'Kill processes':    ('Alt+Shift+K', self, self.terminate_all_workers),
            'Edit destinations': ('Alt+Shift+D', self, partial(self.open_editor, self.destinations)),
            'Edit origins':      ('Alt+Shift+O', self, partial(self.open_editor, self.origins)),
            'Edit setups':       ('Alt+Shift+S', self, partial(self.open_editor, self.setups)),
            'Clear log':         ('Alt+Shift+C', self, self.wipe_log),
            'Save log':          ('Alt+Shift+L', self, self.save_log),
        }
        for key in self.shortcuts:
            QShortcut(*self.shortcuts[key])
        
        # multithreading settings
        self.threadpool = QThreadPool.globalInstance()

    def _button_constructor(self, name: str, descriptor: str, action: Callable) -> None:
        """Constructor for a QToolButton for the IOW-ESM UI.

        Args:
            name (str): internal name of the button
            descriptor (str): display name of the button
            action (Callable): function, method or callable to be used as button action
        """
        self.buttons[name] = QToolButton()
        init_widget(self.buttons[name], name)
        self.buttons[name].setText(descriptor)
        self.buttons[name].clicked.connect(action)
        self.buttons[name].setFixedHeight(30)
        self.buttons[name].setMinimumWidth(self.layout['min_button'])
        
    def _dropdown_constructor(self, name: str, descriptors: list, post: Callable) -> None:
        """Constructor for a dropdown list for the IOW-ESM UI.

        Args:
            name (str): internal name of the button opening the dropdown
            descriptors (list): display name of the button
            post (Callable): function, method or callable to be used as selection action, must take a str as input
        """
        self.dropdowns[name] = QComboBox()
        init_widget(self.dropdowns[name], name)
        self.dropdowns[name].addItems(descriptors)
        self.dropdowns[name].currentTextChanged.connect(
            partial(self._evaluate_dropdown, name, post)
        )
        self.dropdowns[name].setStyleSheet("QComboBox {combobox-popup: 0;}")
        self.dropdowns[name].setMaxVisibleItems(10)
        self.dropdowns[name].setFixedHeight(30)
        
    def _menu_constructor(self, name: str, descriptor: str, items: list, action: Callable) -> None:
        """Constructor for a selection menu with checkboxes for the IOW-ESM UI

        Args:
            name (str): internal name of the menu
            descriptor (str): menu display text
            items (list): list of items to appear in the menu as checkboxes
            action (Callable): function, method or callable to be used as selection action for each item, must take a str as input
        """
        # init menu dict
        self.menus[name] = {}

        # build button         
        self._button_constructor(
            name=name,
            descriptor=descriptor,
            action=self._dummy      # menu buttons do not need actions
        )
        self.buttons[name].setFixedWidth(self.layout['wide_button'])
        
        # set menu widget; relies on inheritence keeping the menu open after using an action
        self.menus[name]['menu'] = QPersistentMenu(self.buttons[name])
        self.menus[name]['menu'].setFixedWidth(self.layout['wide_button'])
        
        # let menu open on button click
        self.buttons[name].setPopupMode(QToolButton.InstantPopup)
        
        # add checkable items to menu
        self.menus[name]['items'] = {}
        for key in items:
            self.menus[name]['items'][key] = self.menus[name]['menu'].addAction(key)
            self.menus[name]['items'][key].setCheckable(True)
            self.menus[name]['items'][key].toggled.connect(partial(action, key))

        # attach menu to button
        self.buttons[name].setMenu(self.menus[name]['menu'])
        self.buttons[name].setStyleSheet("QMenu { menu-scrollable: 1; }");

        
    def _subtitle_constructor(self, name: str, descriptor: str) -> None:
        """Construct a QLabel instance for a given (sub)title. Used to create headlines in the IOW-ESM UI.

        Args:
            name (str): internal name of the headline widget
            descriptor (str): headline / subtitle text
        """
        self.subtitles[name] = QLabel(descriptor)
        init_widget(self.subtitles[name], name)

        self.subtitles[name].setFont(self.subtitle_font)
        self.subtitles[name].setAlignment(Qt.AlignLeft | Qt.AlignTop)

    def _build_widget_titles(self) -> None:
        """Create the title widget for IOW-ESM UI.
        """
        self.title_label = QLabel("IOW-ESM")
        init_widget(self.title_label, "title_label")

        self.title_label.setFont(self.title_font)
        self.title_label.setAlignment(Qt.AlignLeft | Qt.AlignTop)

        self._subtitle_constructor('main_subtitle', "A modular regional Earth System Model.")
        
    def _build_widget_logo(self) -> None:
        """Builds the logo widget for the title of the IOW-ESM UI.
        """
        self.logo_widget = QWidget()
        init_widget(self.logo_widget, "Logo")
        self.logo_layout = QHBoxLayout(self.logo_widget)
        
        self.esm_logo = QSvgWidget()
        self.esm_logo.load("./backend/assets/logo_iow-esm.svg")
        self.esm_logo.setFixedWidth(60)
        self.esm_logo.setFixedHeight(60)
        self.logo_layout.addWidget(self.esm_logo)
        
        self.logo_layout.addStretch()
        
        self.iow_logo = QSvgWidget()
        self.iow_logo.load("./backend/assets/logo_iow_deutsch_white_small.svg")
        self.iow_logo.setFixedWidth(138)
        self.iow_logo.setFixedHeight(60)
        self.logo_layout.addWidget(self.iow_logo)
        
        self.logo_widget.setFixedWidth(230)
        
    def _build_widget_log_textblock(self) -> None:
        """Setup the widget for the log pane.
        """

        # set empty log and counter
        self.log = ''

        # make log reader
        self.log_textbrowser = QTextEdit()
        self.log_textbrowser.setReadOnly(True)
        # self.log_textbrowser.setLineWrapMode(QTextEdit.WidgetWidth)
        self.log_textbrowser.setLineWrapMode(QTextEdit.LineWrapMode.NoWrap)
        init_widget(self.log_textbrowser, "log browser")
        
        self.log_textbrowser.setMinimumWidth(800)
        self.log_textbrowser.setFont(QFontDatabase.systemFont(QFontDatabase.FixedFont))

    def _build_block_header(self) -> None:
        """Builds the header block.
        """

        # build widgets
        self._build_widget_titles()   # main title
        self._build_widget_logo()    # logo

        # build 
        self.header_widget = QWidget()
        self.header_layout = QGridLayout(self.header_widget)
        self.header_layout.addWidget(self.title_label, 0, 0, 1, 2)
        self.header_layout.addWidget(self.subtitles['main_subtitle'], 1, 0, 1, 2)
        self.header_layout.addWidget(self.logo_widget, 0, 2, 2, 1)
        self.header_widget.setFixedHeight(100)

    def _build_block_destinations(self) -> None:
        """Builds the destination block.
        """

        # TODO: implement adding targets.
        # get destinations from file, construct descriptors
        self.destinations.get_content()
        descriptors = ['Choose destination'] + list(self.destinations.keys())
        
        # build widgets
        self._subtitle_constructor('destination_title', 'Destinations')
        self._dropdown_constructor(
            name='destination',
            descriptors=descriptors,
            post=self.update_deployed_inputs
        )
        self._button_constructor(
            name='destination_editor',
            descriptor='Edit',
            action=partial(self.open_editor, self.destinations)
        )

        # build layout
        self.destination_widget = QWidget()
        self.destination_layout = QGridLayout(self.destination_widget)
        self.destination_layout.addWidget(self.subtitles['destination_title'], 0, 0, 1, 2)
        self.destination_layout.addWidget(self.dropdowns['destination'], 1, 0, 1, 1)
        self.destination_layout.addWidget(self.buttons['destination_editor'], 1, 1, 1, 1)
        self.destination_widget.setFixedHeight(80)

    def _build_block_builder(self) -> None:
        """Create the build block.
        """
        
        build_mode_descriptors = ['Build mode', 'release', 'debug']
        rebuild_descriptors = ['Rebuild mode', 'fast', 'rebuild']
        
        self._subtitle_constructor('build_title', 'Build')
        self._menu_constructor(
            name='target',
            descriptor='Choose targets',
            items=list(self.origins.keys()),
            action=partial(self.update_current_list, 'targets')
        )
        self._dropdown_constructor(
            name='build mode',
            descriptors=build_mode_descriptors,
            post=self.update_build_config
        )
        self._dropdown_constructor(
            name='rebuild mode',
            descriptors=rebuild_descriptors,
            post=self.update_build_config
        )
        self._button_constructor(
            name='origins_editor',
            descriptor='Edit origins',
            action=partial(self.open_editor, self.origins)
        )
        self._button_constructor(
            name='build',
            descriptor='Build',
            action=self.build_targets
        )
        self._button_constructor(
            name='clone',
            descriptor='Clone',
            action=self.clone_targets
        )
        
        # set initial value for dropdowns to None (not chosen)
        for dropdown in ['build mode', 'rebuild mode']:
            self.current[dropdown] = ''
        
        
        # build layout
        self.build_widget = QWidget()
        self.build_layout = QGridLayout(self.build_widget)
        self.build_layout.addWidget(self.subtitles['build_title'], 0, 0, 1, 4)
        self.build_layout.addWidget(self.buttons['target'], 1, 0, 1, 2)
        self.build_layout.addWidget(self.dropdowns['build mode'], 2, 0, 1, 1)
        self.build_layout.addWidget(self.dropdowns['rebuild mode'], 2, 1, 1, 1)
        self.build_layout.addWidget(self.buttons['origins_editor'], 1, 3, 1, 1)
        self.build_layout.addWidget(self.buttons['clone'], 1, 4, 1, 1)
        self.build_layout.addWidget(self.buttons['build'], 2, 4, 1, 1)
        self.build_widget.setFixedHeight(120)

    def _build_block_setups(self) -> None:
        """Build the setup block.
        """
        
        self._subtitle_constructor('setups_title', 'Setups')
        self._button_constructor(
            name='setups_getinfo',
            descriptor='Get info',
            action=self.display_setups_info
        )
        self._button_constructor(
            name='setups_deploy',
            descriptor='Deploy',
            action=self.deploy_setups
        )
        self._button_constructor(
            name='setups_editor',
            descriptor='Edit setups',
            action=partial(self.open_editor, self.setups)
        )
        self._button_constructor(
            name='setups_archiver',
            descriptor='Archive',
            action=self.archive_setups
        )
        self._menu_constructor(
            name='setup_chooser',
            descriptor='Choose setups',
            items=self.available_setups,
            action=partial(self.update_current_dictionary, 'setups')
        )
        
        # TODO: activate archive button after implementing it properly
        self.buttons['setups_archiver'].setDisabled(True)
        
        self.setups_widget = QWidget()
        self.setups_layout = QGridLayout(self.setups_widget)
        self.setups_layout.addWidget(self.subtitles['setups_title'], 0, 0, 1, 4)
        self.setups_layout.addWidget(self.buttons['setup_chooser'], 1, 0, 1, 2)
        self.setups_layout.addWidget(self.buttons['setups_editor'], 1, 2, 1, 1)
        self.setups_layout.addWidget(self.buttons['setups_getinfo'], 1, 3, 1, 1)
        self.setups_layout.addWidget(self.buttons['setups_archiver'], 2, 2, 1, 1)
        self.setups_layout.addWidget(self.buttons['setups_deploy'], 2, 3, 1, 1)
        self.setups_widget.setFixedHeight(120)
        
    def _build_block_run(self) -> None:
        """Build the run block. This block also includes the generation and starting of PPEs.
        """
        
        self._subtitle_constructor('run_title', 'Run')
        
        descriptors = ['Sync destination'] + list(self.destinations.keys())
        self._dropdown_constructor(
            name='sync destination',
            descriptors=descriptors,
            post=self._dummy
        )
        self._button_constructor(
            name='run_syncer',
            descriptor='Sync',
            action=self.sync
        )
        # self._button_constructor(
        #     name='edit_ensemble_config',
        #     descriptor='Edit ensemble config',
        #     action=partial(self.open_editor, backend.auxclasses.FormatText(
        #         glob.glob(f'{backend.globals.root_dir}/plugins/*.py')[0],
        #         name='test',
        #         parent=self,
        #         post_edit=self._dummy
        #     ))
        # )
        self._button_constructor(
            name='run_launcher',
            descriptor='Run',
            action=self.start_runs
        )
        self._menu_constructor(
            name='run_input_chooser',
            descriptor='Choose inputs',
            items=self.deployed_inputs,
            action=partial(self.update_current_list, 'inputs')
        )
        
        for button in ['run_input_chooser', 'run_launcher']:  # 'edit_ensemble_config'
            self.buttons[button].setFixedWidth(self.layout['wide_button'])
        self.dropdowns['sync destination'].setFixedWidth(self.layout['wide_button'])
        
        self.run_settings = QGroupBox("Run settings")
        self.run_settings_layout = QGridLayout(self.run_settings)
        
        # checkbox
        self.run_mapping_checkbox = QCheckBox("Create mappings")
        init_widget(self.run_mapping_checkbox, "create mappings")
        
        # # radio button settings
        # self.run_radio_single = QRadioButton("Single run")
        # init_widget(self.run_radio_single, "run_radio_single")
        # self.run_radio_ensemble = QRadioButton("Ensemble")
        # init_widget(self.run_radio_ensemble, "run_radio_ensemble")
        # self.run_radio_single.toggled.connect(self.buttons['edit_ensemble_config'].setDisabled)
        # self.run_radio_single.setChecked(True)
        
        # layout of settings box
        self.run_settings_layout.addWidget(self.run_mapping_checkbox, 0, 0, 1, 1)
        # self.run_settings_layout.addWidget(self.run_radio_single, 1, 0, 1, 1)
        # self.run_settings_layout.addWidget(self.run_radio_ensemble, 1, 1, 1, 1)
        
        self.run_widget = QWidget()
        self.run_layout = QGridLayout(self.run_widget)
        self.run_layout.addWidget(self.subtitles['run_title'], 0, 0, 1, 4)
        self.run_layout.addWidget(self.buttons['run_input_chooser'], 1, 0, 1, 2)
        # self.run_layout.addWidget(self.buttons['edit_ensemble_config'], 2, 0, 1, 2)
        self.run_layout.addWidget(self.buttons['run_launcher'], 2, 0, 1, 2)
        self.run_layout.addWidget(self.dropdowns['sync destination'], 3, 0, 1, 2)
        self.run_layout.addWidget(self.buttons['run_syncer'], 3, 2, 1, 2)
        self.run_layout.addWidget(self.run_settings, 1, 2, 2, 2)
        
        self.run_widget.setFixedHeight(160)
        
    def _build_block_post(self) -> None:
        """Build the post processing block.

        This block currently includes a button to start the analysis.
        Future implementations should expand this block to include more post-processing features.
        
        TODO: Implement additional post-processing functionalities.
        """
        self._subtitle_constructor('post_title', 'Post processing')
        
        # TODO: implement post production window
        self._button_constructor(
            name='open_post_processor',
            descriptor='Start analysis',
            action=self._dummy
        )
        self.buttons['open_post_processor'].setFixedWidth(self.layout['wide_button'])
        
        self.post_widget = QWidget()
        self.post_layout = QGridLayout(self.post_widget)
        self.post_layout.addWidget(self.subtitles['post_title'], 0, 0, 1, 2)
        self.post_layout.addWidget(self.buttons['open_post_processor'], 1, 0, 1, 2)
        self.post_widget.setFixedHeight(80)
        
    def _build_block_log(self) -> None:
        """Build the log pane block. Contains the window for the log messages.
        """
        
        self._subtitle_constructor('log_title', 'Logs')
        self._build_widget_log_textblock()

        self.log_widget = QWidget()
        self.log_layout = QVBoxLayout(self.log_widget)
        self.log_layout.addWidget(self.subtitles['log_title'])
        self.log_layout.addWidget(self.log_textbrowser)

    def _build_block_footer(self) -> None:
        """Build the footer block.
        """

        self._button_constructor(           # TODO: add confirmation dialog
            name='footer_wiper',
            descriptor='Wipe log',
            action=self.wipe_log
        )
        self._button_constructor(
            name='footer_saver',
            descriptor='Save log',
            action=partial(self.save_log, './LOG.txt')
        )
        self._button_constructor(
            name='footer_linker',
            descriptor='Documentation',
            action=self.open_documentation
        )
        self._button_constructor(
            name='footer_shortcuts',
            descriptor='Shortcuts',
            action=self.open_shortcuts
        )
        self._button_constructor(
            name='footer_theme_toggle',
            descriptor='Light theme',
            action=self.toggle_display_mode
        )
        self._button_constructor(
            name='footer_thread_killer',
            descriptor='Kill threads',
            action=self.terminate_all_workers
        )
        
        self.worker_display = QLabel(f'{0}')
        init_widget(self.worker_display, 'Worker count')
        self.worker_display.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.worker_display.setFixedHeight(30)
        self.worker_display.setFixedWidth(40)
        

        self.footer_widget = QWidget()
        self.footer_layout = QHBoxLayout(self.footer_widget)
        self.footer_layout.addWidget(self.worker_display)
        self.footer_layout.addWidget(self.buttons['footer_thread_killer'])
        self.footer_layout.addWidget(self.buttons['footer_linker'])
        self.footer_layout.addWidget(self.buttons['footer_shortcuts'])
        self.footer_layout.addWidget(self.buttons['footer_theme_toggle'])
        self.footer_layout.addStretch(1)
        self.footer_layout.addWidget(self.buttons['footer_wiper'])
        self.footer_layout.addWidget(self.buttons['footer_saver'])
        self.footer_widget.setFixedHeight(40)
       
    @Slot()
    def _evaluate_dropdown(self, name: str, post: Callable, val = '') -> None:
        """Evaluate a chosen dropdown value and modify the corresponding "current".

        Args:
            name (str): internal name of dropdown
            post (Callable): function, method, or callable to be called after evalution.
            val (str, optional): value to be set in evaluation. If not set, val will be given by current dropdown choice. Defaults to ''.
        """
        
        # do not evaluate after clearing all options
        if not self.dropdowns[name].currentIndex() == -1:
            # if new value is not explicitely given, get the changed value
            if not val:
                val = self.dropdowns[name].currentText()
            
            if not val or self.dropdowns[name].currentIndex() == 0:
                # if the value is empty, or the index is zero (Choose content message)
                # reset the current value
                self.current[name] = ''
                self.eh.report_error('main', 'info', f'Resetting current {name}')
            else:
                # set new current value and run the post hook
                self.current[name] = val
                self.eh.report_error('main', 'info', f'Changed {name} to {val}')
            
            post()
    
    def _rebuild_menu(self, name: str, items: list, action: Callable) -> None:
        """Rebuilds a menu with the given items.

        Args:
            name (str): internal name of menu
            items (list): list of items to appear as checkboxes
            action (Callable): action to be called when choosing an option. Must take a str as argument.
        """
        
        # clear menu
        self.menus[name]['menu'].clear()
        
        # rebuild items
        self.menus[name]['items'] = {}
        for key in items:            
            self.menus[name]['items'][key] = self.menus[name]['menu'].addAction(key)
            self.menus[name]['items'][key].setCheckable(True)
            self.menus[name]['items'][key].toggled.connect(partial(action, key))
        
        # prevent menu from opening after update
        self.menus[name]['menu'].hide()
    
    @Slot()
    def display_setups_info(self) -> None:
        """Get info about setups. Wrapper for backend functionality.
        """
        
        # get all infos about setups
        if self.current['setups']:
            infos = self.intfc.get_setups_info(self.current['setups'])
            
            for info in infos:
                setattr(info, 'parent', self)
                setattr(info, 'post_edit', self._dummy)
            
            if not any([info.text for info in infos]):
                self.eh.report_error('main', 'warning', 'Did not receive any setup info.')
            else:
                # iterate over list of auxclasses.FormatText objects
                for info in infos:
                    # open editor if the info has any content
                    if info.text:
                        self.open_editor(info)
        else:
            self.eh.report_error('main', 'warning', 'No setup selected, not opening infos.')
    
    @Slot()
    def deploy_setups(self) -> None:
        """Deploy setup configurations. Wrapper for backend functionality.
        """
        
        procs = self.intfc.deploy_setups(               
            self.current['destination'],        # destination ti deploy to
            list(self.current['setups'].keys()) # setups to deploy
        )
        
        # execute non-blocking process
        for proc in procs:
            self.execute(
                proc,                                   # func
                False,                                  # block
                True,                                   # verbose
                self.update_deployed_inputs,            # post
                'setup deployment'                      # message
            )
    
    @Slot()
    def archive_setups(self) -> None:
        """Archive setups. Wrapper for backend functionality.
        
        TODO: implement robust archiving
        """
        self.intfc.archive_setup(
            self.current['destination'],
            self.deploy_setups,
            'path/whoch/needs/input'
        )
        
    @Slot()
    def sync(self) -> None:
        """Sync model I/O to a second destination. Wrapper for backend functionality.
        """
        if not self.current['destination']:
            self.eh.report_error('main', 'warning', 'Source destination for sync not set.')
            return
        
        if not self.current['sync destination']:
            self.eh.report_error('main', 'warning', 'Target destination for sync not set.')
            return
        
        self.execute(
            self.intfc.sync_now(                    # func
                self.current['destination'],        # destination to sync from
                self.current['sync destination']    # destination to sync to
            ),
            False,                                  # block
            True,                                   # verbose
            self._dummy,                            # post
            'output synchronization',               # message
        )
    
    @Slot()
    def toggle_display_mode(self):
        """Toggles display mode of the UI (dark / light).
        """
        if self.dark:
            QApplication.setPalette(QPalette(QColor(backend.globals.IowColors.grey4)))
            self.buttons['footer_theme_toggle'].setText('Dark theme')
            self.iow_logo.load("./backend/assets/logo_iow_deutsch_blue_small.svg")
            self.dark = False
        else:
            QApplication.setPalette(QPalette(QColor(backend.globals.IowColors.blue1)))
            self.buttons['footer_theme_toggle'].setText('Light theme')
            self.iow_logo.load("./backend/assets/logo_iow_deutsch_white_small.svg")
            self.dark = True
        
    @Slot()
    def wipe_log(self) -> None:
        """Remove the log from the log pane.
        """
        # self.log = '>>> CLEANED LOG <<<\n'
        self.log = ''
        self.log_textbrowser.setPlainText(self.log)

    @Slot()
    def save_log(self, logpath='./frontend.log') -> None:
        """Save the current log messages to a file.

        Args:
            logpath (str, optional): File path to save the log to. Defaults to './frontend.log'.
        """

        with open(logpath, 'a') as logger:
            logger.write(self.log)

        self.eh.report_error('main', 'info', f'Saved log to {logpath}')

    @Slot()
    def write_to_log(self, source: str, level: str, message: str | bytes) -> None:
        """Writes a message to the log file.

        Args:
            source (str): source of the message (backend / frontend / interface, ...)
            level (str): message level, should be in (info, warning, fatal, stdout)
            message (str): message to be written to log
        """
        
        # get scroll bar
        hor_scroll_bar = self.log_textbrowser.horizontalScrollBar()
        ver_scroll_bar = self.log_textbrowser.verticalScrollBar()
        
        # get current time
        current_time = datetime.now().strftime(
            '%Y-%m-%d %H:%M:%S'
        )  # should always have the same length.
        
        # convert to string
        if type(message) == str:
            lines = message.split('\n')
        else:
            lines = [
                ansi_escape.sub(b'', line.strip()).decode('latin-1').strip()
                # str(line.strip())
                for line in message.split(b'\n') if line.strip()
            ]
        
        # append message to log text
        tba = ''
        for line in lines:
            tba += f"{current_time} [{source:<6}] ({level:<7}) {line}\n"
            self.log += f'{tba[:-1]}\n'

        # append message to log window, remove last newline
        self.log_textbrowser.append(tba[:-1])
        
        # scroll to left and last line
        ver_scroll_bar.setValue(ver_scroll_bar.maximum())
        hor_scroll_bar.setValue(0)

    @Slot()
    def open_editor(self, content: object) -> None:
        """Open the editor window.

        Args:
            content (object): object from the backend.auxclasses
        """
        self.eh.report_error('main', 'info', f'Opening editor window ({content.name}).')
        self.editor = frontend.editor.Editor(self, content)
        self.editor.show()

    @Slot()
    def open_documentation(self) -> None:
        """Open the documentation in a browser.
        """
        QDesktopServices.openUrl('https://sven-karsten.github.io/iow_esm/intro.html')

    @Slot()
    def open_shortcuts(self) -> None:
        """Open all available keyboard shortcuts. Currently a dummy.
        """
        # TODO: set up keyboard shortcuts and window for explanations
        self.eh.report_error('main', 'info', f'Opening shortcut help.')
        self.help = frontend.main.Shortcuts(self, self.shortcuts)
        self.help.show()

    @Slot()
    def update_destinations(self) -> None:
        """Update the dropdown list of available destinations.
        """
        
        self.destinations.get_content()
        
        for key in ['destination', 'sync destination']:
            self.dropdowns[key].clear()                               # calls dropdown post hook
            self.dropdowns[key].addItems([f'Choose {key.capitalize()}.'])     # calls dropdown post hook a second time
            self.dropdowns[key].addItems(list(self.destinations.keys()))
        
        self.eh.report_error('main', 'info', 'Updating destinations.')
        
    @Slot()
    def update_available_setups(self) -> None:
        """Update the menu for available setups.
        """
        # read file
        self.setups.get_content()
        
        # update list of available setups
        self.available_setups = list(self.setups.keys())
        
        # rebuild menu
        self._rebuild_menu('setup_chooser', self.available_setups, partial(self.update_current_dictionary, 'setups'))
        
        # reset dictionary of currently selected setups
        self.current['setups'] = {}
        
        # report
        self.eh.report_error('main', 'info', 'Updating available setups.')
        
    @Slot()
    def update_deployed_inputs(self) -> None:
        """Updates the selected inputs for the current destination.
        """
        
        if not self.current['destination']:
            self.deployed_inputs = []
            self._rebuild_menu('run_input_chooser', self.deployed_inputs, partial(self.update_current_list, 'inputs'))
            self.eh.report_error('main', 'info', f'Resetting available inputs.')
        else:
            # find available deployed inputs
            self.deployed_inputs = self.intfc.check_for_available_inputs(self.destinations, self.current['destination'])
            
            # rebuild menu
            self._rebuild_menu('run_input_chooser', self.deployed_inputs, partial(self.update_current_list, 'inputs'))
            
            # reset dictionary of currently selected inputs
            self.current['inputs'] = []
            
            # report
            self.eh.report_error('main', 'info', f'Updating available inputs at destination {self.current["destination"]}.')
    
    @Slot()
    def update_build_config(self) -> None:
        """Update the build configuration.
        """
        
        self.current['build_config'] = f'{self.current["build mode"]} {self.current["rebuild mode"]}'
        # self.eh.report_error('main', 'info', f'Updating build config to {self.current['build_config']}.')

    @Slot()
    def update_available_targets(self) -> None:
        """Update the list or origins / targets.
        """
        
        # make sure the dictionary is up to date
        self.origins.get_content()
        
        # rebuild menu
        self._rebuild_menu('target', list(self.origins.keys()), partial(self.update_current_dictionary, 'origins'))
        
        # reset current dicionary
        self.current['origins'] = {}
        
        # report
        self.eh.report_error('main', 'info', f'Updating available origins.')
        
    @Slot()
    def update_current_dictionary(self, dictname: str, name: str, state: bool) -> None:
        """Update the dictionary of currents (dictionary entries) with the given name, dependent on the state.

        Args:
            dictname (str): internal name of dictionary entry in current.
            name (str): key to be updated
            state (bool): resset if False, set if True
        """
        
        dictionary = self.__getattribute__(dictname)
        
        if state:
            if name in dictionary:
                self.current[dictname][name] = dictionary[name]
                self.eh.report_error('main', 'info', f'Adding {name} to current {dictname}.')
            else:
                self.eh.report_error('main', 'warning', f'{name} not present in available {dictname}.')
        else:
            if name in self.current[dictname]:
                del self.current[dictname][name]
                self.eh.report_error('main', 'info', f'Deleting {name} from current {dictname}.')
            else:
                self.eh.report_error('main', 'warning', f'{name} not present in available {dictname}.')
    
    @Slot()
    def update_current_list(self, listname: str, name: str, state: bool) -> None:
        """Update the dictionary of currents (list entries) with the given name, dependent on the state.

        Args:
            listname (str): internal name of list entry in current.
            name (str): key to be updated
            state (bool): resset if False, set if True
        """
        if state:
            if not name in self.current[listname]:
                self.current[listname].append(name)
                self.eh.report_error('main', 'info', f'Adding {name} to current {listname}.')
        else:
            try:
                self.current[listname].remove(name)
                self.eh.report_error('main', 'info', f'Removing {name} from current {listname}.')
            except:
                self.eh.report_error('main', 'warning', f'{name} not present in current {listname}.')
    
    @Slot()
    def build_targets(self) -> None:
        """Build the targets on the destination machine.
        """
        if not self.current['targets']:
            self.eh.report_error('main', 'warning', 'No target set, nothing to build.')
            return
        
        if not self.current['build_config'] or 'mode' in self.current['build_config']:
            self.eh.report_error('main', 'fatal', 'Build config not specified, cannot build.')
            return
        
        if not self.current['destination']:
            self.eh.report_error('main', 'fatal', 'No destination set, aborting build.')
            return
        
            
        for target in self.current['targets']:
            if not glob.glob(f'{target}/build.sh'):
                self.eh.report_error('main', 'fatal', f'Source of {target} not found, cannot build.')
            else:
                try:
                    self.execute(
                        self.intfc.build_origin(                    # func
                            target, self.current['destination'],
                            self.current['build_config']
                        ),
                        False,                                      # block
                        True,                                       # verbose
                        self._dummy,                                # post
                        'build process',                            # message
                    )
                except KeyError:
                    self.eh.report_error('main', 'fatal', '{target} not in origins, cannot build.')
    
    @Slot()
    def clone_targets(self) -> None:
        """Clone the origins / targets to local machine. Wrapper for backend functionality.
        """
        
        if not self.current['targets']:
            self.eh.report_error('main', 'warning', 'No target set, nothing to clone.')
            return
        
        for target in self.current['targets']:
            try:
                self.execute(
                    self.intfc.clone_origin(target),            # func
                    True,                                       # block (clone is not thread safe due to temp execution name)
                    True,                                       # verbose
                    partial(self.intfc.check_origin, target),   # post (blocking)
                    'git clone'                                 # message
                )
                
            except KeyError:
                self.eh.report_error('main', 'fatal', '{target} not in origins, cannot clone.')
    
    @Slot()
    def start_runs(self) -> None:
        """Start all the runs on current destination. Wrapper for backend functionality.
        """
        
        self.eh.report_error('main', 'info', f'Starting run at {self.current["destination"]} with inputs {self.current["inputs"]}')
        
        if self.run_mapping_checkbox.isChecked():
            self.eh.report_error('main', 'info', f'Creating grid mappings before run.')
            
        self.execute(
            self.intfc.run(                             # func for worker execution
                self.current['destination'],            # destination
                self.current['sync destination'],       # sync destination
                self.run_mapping_checkbox.isChecked(),  # prepare, generate mapping
                self.current['inputs']                  # inputs
            ),
            False,                                      # block
            True,                                       # verbose
            self._dummy,                                # post
            'model run',                                # message
        )
                    
    @Slot()
    def _dummy(self, *args, **kwargs) -> None:
        """A dummy method.
        """
        pass
    
    @Slot()
    def run_in_background(self, task: Callable, verbose: bool, post: Callable) -> None:
        """Run a task in the background.

        Args:
            task (Callable): task to be run, must be a partial or lambda of a 'subprocess.Popen' instance
            verbose (bool): return the stdout output to the log
            post (Callable): callable to execute once the task is done
        """
        
        worker = frontend.workers.WorkerRunner(task, verbose)
        
        worker.signals.started.connect(partial(self.write_to_log, 'worker', 'info', 'started in background.'))
        worker.signals.error.connect(self.eh.report_stack)
        if verbose:
            worker.signals.result.connect(self.write_worker_result)
            worker.signals.stdout.connect(partial(self.write_to_log, 'cmd', 'stdout'))
        else:
            worker.signals.stdout.connect(self._dummy)
            worker.signals.result.connect(self._dummy)
            
        worker.signals.finished.connect(partial(self.terminate_worker, worker, post))
        
        self.signals.terminator.connect(worker.terminate)

        self.threadpool.start(worker)
        
        # update number of workers
        self.signals.mod_worker.emit()
                
    @Slot()
    def terminate_worker(self, worker: frontend.workers.WorkerRunner, post: Callable, message: str) -> None:
        """Terminate a worker and disconnect all signals. Also updates the count of workers and is always connected to the finish signal of the workers.

        Args:
            worker (frontend.workers.WorkerRunner): worker process to be terminated
            post (Callable): callable to execute after termination (both successful or abort)
            message (str): message of termination for log
        """
        
        # disconnect all signals
        if worker.isRunning():
            worker.terminate()
            
        # update number of workers display
        # also deletes worker from dictionary
        self.signals.mod_worker.emit()
        
        # write stdout to log
        level = 'info'
        if message:
            self.write_to_log('worker', level, f'worker terminated with {message}.')
        else:
            self.write_to_log('worker', level, 'worker terminated withtout message.')
    
        # run post hook
        post()
    
    @Slot()
    def terminate_all_workers(self) -> None:
        """Terminate all running workers.
        """
        if self.threadpool.activeThreadCount() == 0:
            self.write_to_log('main', 'info', f'No workers running to terminate.')
        else:
            self.write_to_log('main', 'warning', f'Terminating all {self.threadpool.activeThreadCount()} workers.')
            self.signals.terminator.emit()
        
        self.signals.mod_worker.emit()
    
    @Slot()
    def write_worker_result(self, result: any) -> None:
        """Write the result of a worker running in the background into the log pane.

        Args:
            result (any): result returned from the worker, will be transferred to a string
        """
        if result:
            level = 'info'
            self.write_to_log('worker', level, f'Returned: {result}')
            
    @Slot()
    def update_num_workers(self) -> None: 
        """Update the number of workers in the threadpool.
        """
        self.worker_display.setText(f'{self.threadpool.activeThreadCount()}')
        
    @Slot()
    def execute(self, func: Callable, block: bool, verbose: bool, post: Callable, message: str, *args, **kwargs) -> None:
        """Execute a function in a background thread. Accepts *args and **kwargs, these are given to the provided callable 'func'.

        Args:
            func (Callable): callable to execute, msust be a partial or lambda of a 'subprocess.Popen' instance to enable process polling
            block (bool): block UI (foreground execution) or send task to background (runnable in QThreadPool)
            verbose (bool): print stdout to log pane
            post (Callable): callable to execute after finishing the execution
            message (str): message to send to pane before blocking execution start
        """
        
        if block:
            # handle execution locally
            if verbose:
                self.eh.report_error('main', 'info', f'Executing {message}.')
            
            proc = func(*args, **kwargs)
            
            try:
                while(proc.poll() is None):
                    if verbose:
                        line = proc.stdout.readline()
                        self.write_to_log('cmd', 'stdout', line)
                        
                # call post action hook
                post()
                
                if verbose:
                    self.eh.report_error('main', 'info', 'Execution finished.')
                    
            except AttributeError:
                self.eh.report_error('main', 'fatal', 'Executed task must be of type "subprocess.Popen"')
                    
        else:
            # handle execution in QRunner
            self.run_in_background(
                partial(func, *args, **kwargs), verbose, post
            )
            
class Shortcuts(QWidget):
    def __init__(self, parent: frontend.main.IowEsmGui, shortcuts: dict) -> None:
        super().__init__()
        self.setWindowTitle("Help")
        # self.setGeometry(150, 150, 400, 35*len(list(shortcuts.keys()))-1)

        # read initial config
        self.parent = parent
        
        # init buttons
        self.buttons = {}

        # build elements
        self._build_display(shortcuts)
        self._button_constructor(
            name='close_button',
            descriptor='Close',
            action=self.close
        )

        # set layout
        self.layout = QGridLayout(self)
        self.layout.addWidget(self.display, 0, 0, 1, 3)
        self.layout.addWidget(self.buttons['close_button'], 1, 2, 1, 1)
        self.setLayout(self.layout)
        
        # set shortcuts
        self.shortcuts = {
            'close_1':  ('Esc',    self, self.close),
            'close_2':  ('Ctrl+W', self, self.close),
        }
        for key in self.shortcuts:
            QShortcut(*self.shortcuts[key])

    def _button_constructor(self, name: str, descriptor: str, action: Callable) -> None:
        self.buttons[name] = QToolButton()
        frontend.main.init_widget(self.buttons[name], name)
        self.buttons[name].setText(descriptor)
        self.buttons[name].clicked.connect(action)
        self.buttons[name].setFixedHeight(30)

    def _build_display(self, shortcuts: dict) -> None:
        
        shortcut_names = list(shortcuts.keys())
        self.display = QTableWidget()
        frontend.main.init_widget(self.display, "shortcut display")
        self.display.setRowCount(len(shortcut_names)-1)
        self.display.setColumnCount(2)
        self.display.setHorizontalHeaderLabels(['Action', 'Shortcut'])
        self.display.horizontalHeaderItem(Qt.AlignCenter)
        self.display.setAlternatingRowColors(True)
        
        header = self.display.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.ResizeMode.Stretch)
        header.setSectionResizeMode(1, QHeaderView.ResizeMode.Stretch)

        for nsc in range(1, len(list(shortcuts.keys()))):
            self.display.setItem(
                nsc-1, 0, QTableWidgetItem(shortcut_names[nsc])
            )
            self.display.setItem(
                nsc-1, 1, QTableWidgetItem(
                    shortcuts[shortcut_names[nsc]][0]
                )
            )
            
        self.display.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.display.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)