#!/bin/bash

line="---------------------------------------------------------------"
marker="#######"

for origin in . `awk '{print $1}' ORIGINS`; do 
    cd $origin
    if [[ $origin == "." ]]; then origin="main"; fi
    echo $line
    echo "$marker $origin $marker$marker"
    echo $line
    echo "Status of the repository:"
    echo $line
    git status
    echo ""
    if [ "$1" == "-v" ]; then
        echo $line
        echo "Last commit:"
        echo $line
        git log --name-status HEAD^..HEAD | cat
        echo $line
    fi
    echo "" 
    cd - > /dev/null 
done