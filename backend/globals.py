# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 14:43:44 2021

@author: Sven Karsten
"""

import os

root_dir = os.getcwd()

class IowColors:
    """Colors from IOW corporate identity.
    """
    blue1 = "#10427a" 
    blue2 = "#305790"
    blue3 = "#6e85b5"
    blue4 = "#a1aed0"
    green1 = "#96d543"
    green2 = "#abdd64"
    green3 = "#cbe999"
    green4 = "#e0f2c0"
    grey1 = "#4c564c"
    grey2 = "#6b766b"
    grey3 = "#9ea89d"
    grey4 = "#c3cac2"
