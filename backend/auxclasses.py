# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 09:30:00 2024

@author: Georg Sebastian Voelker
"""

from frontend.decorators import IowEsmFrontendDecorator
import csv

class FormatText(dict):
    """A dictionary subclass for text format IOW-ESM configs, inherits from python dictionary.

    Args:
        dict (dictionary): standard dictionary class
    """
    def __init__(self, path: str, name: str, parent: object, post_edit: None, text='') -> None:
        """A dictionary subclass for text format IOW-ESM configs.

        Args:
            path (str): path to file, must be local. If not, a text must be supplied.
            name (str): name of object
            parent (object): parent to the object, (typically the frontend or the interface object)
            post_edit (callable, optional): Action to run after editing the content. Defaults to None.
            text (str, optional): Supplied initial text. Defaults to '', in which case the text will be read from the supplied file.
        """
        self.path = path
        self.name = name
        self.parent = parent
        self.text = text
        self.post_save = post_edit

        self.get_content(text)
     
    @IowEsmFrontendDecorator()
    def get_content(self, text='') -> None:
        """Set the text content of the object.

        Args:
            text (str, optional): Text to be set, if not supplied, it is read from the file path. Defaults to ''.
        """
        # reset current dict
        keys = list(self.keys())
        for key in keys:
            del self[key]
        
        try:
            if text:
                self.text = text
            else:
                with open(self.path, 'r') as file:
                    self.text = "".join(file.readlines())
            
            for line in self.text.split('\n'):
                # skip comments
                if len(line) == 0:
                    continue
                if line.lstrip()[0] == "#":
                    continue
                
                # separate line into keyword and value
                try:
                    split_line = line.split(' ')                    # use space for splitting
                    self[split_line[0]] = "".join(split_line[1:])   # use rest of line as value
                except:
                    # ignore empty or keyword only lines
                    pass
        except FileNotFoundError:
            self.parent.eh.report_error('editor','warning', f'Text file at {self.path} not found.')
            self.text = ''
        
    @IowEsmFrontendDecorator()
    def save_content(self, text: str='') -> None:
        """Save the text content to the file path.

        Args:
            text (str, optional): Text to be saved to the file. Defaults to '', if empty, the current dictionary content will be saved. otherwise the supplied text will be set as the current content before saving.
        """

        if text:
            # if new text is supplied replace full text (self.text) and config dict (self[key])
            self.get_content(text)
        try:
            with open(self.path, 'w') as file:
                file.write(self.text)
            
            # report after writing (or don't if it fails)
            self.parent.eh.report_error('editor', 'info',f'Saving {self.name} to {self.path}.') 
            
        except FileNotFoundError:
            self.parent.eh.report_error('editor','warning', f'Not able to save text file to {self.path}.')
            
        # run post hook
        if self.post_save:
            self.post_save()


class FormatTable(dict):
    """A dictionary subclass for tabular format IOW-ESM configs, inherits from python dictionary.

    Args:
        dict (dictionary): standard dictionary class
    """
    def __init__(self, path: str, name: str, parent: object, post_edit=None, data=None) -> None:
        """A dictionary subclass for tabular format IOW-ESM configs.

        Args:
            path (str): path to file, must be local. If not, a text must be supplied.
            name (str): name of object
            parent (object): parent to the object, (typically the frontend or the interface object)
            post_edit (callable, optional): Action to run after editing the content. Defaults to None.
            data (iterable, optional): Supplied initial data. Defaults to None, in which case the data will be read from the supplied file.
        """
        self.path = path
        self.name = name
        self.parent = parent
        self.data = ''
        self.post_save = post_edit
        
        self.get_content(data)
    
    @IowEsmFrontendDecorator()
    def get_content(self, data=None) -> None:
        """Set the text content of the object.

        Args:
            data (iterable, optional): Data to be set, if not supplied, it is read from the file path. Defaults to None.
        """
        
        if data:
            self.data = data
        else:
            try:
                data = []
                with open(self.path, 'r') as file:
                    csvFile = csv.reader(file)
                    for lines in csvFile:
                        data.append(lines)
                self.data = data
            except FileNotFoundError:
                # TODO: make this content more general if needed (for large PPEs).
                self.parent.eh.report_error('editor', 'warning', f'CSV file at {self.path} not found. Using empty table.')
                self.data = [['', '', '', '']] * 50
        
        # save every row in dictionary
        for row in self.data:
            self[row[0]] = row[1:]

    @IowEsmFrontendDecorator()
    def save_content(self, data=None) -> None:
        """Save the content to the file path.

        Args:
            data (iterable, optional): DataText to be saved to the file. Defaults to None, if empty, the current dictionary content will be saved. otherwise the supplied data will be set as the current content before saving.
        """

        if data:
            self.data = data
        
        try:
            with open(self.path, 'w', newline='') as file:
                writer = csv.writer(file)
                writer.writerows(self.data)
            self.parent.eh.report_error('editor', 'info', f'Saving {self.name} to {self.path}.')
        except FileNotFoundError:
            self.parent.eh.report_error('editor', 'warning', f'Cannot save CSV file to {self.path}.')
            
        # run post hook
        if self.post_save:
            self.post_save()
