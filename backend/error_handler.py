# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 14:43:21 2021
Modified on Mon Nov 25 09:30:00 2024

@author: Sven Karsten, Georg Sebastian Voelker
"""

import backend
import json

class IowEsmErrorLevels():
    """Class to handle error levels.
    """
    fatal = "fatal"
    warning = "warning"
    info = "info"
    stdout = "stdout"

class IowEsmErrors():
    """Create a class instance with default errors and warnings.
    """
    clone_origins = ('git', IowEsmErrorLevels.fatal, "Not all origins could be cloned!")
    build_origins_first_time = ('compiler', IowEsmErrorLevels.fatal, "Not all origins could be built!")
    deploy_setups_first_time = ('ssh', IowEsmErrorLevels.fatal, "Not all setups could be deployed!")
    destination_not_set = ('intfc', IowEsmErrorLevels.warning, "Destination not set.")

class IowEsmErrorHandler():
    """IOW-ESM error handler class. Needs to be connected to a parent containing the 'write_to_log' method.
    """
    def __init__(self, parent: object) -> None:
        """Initialize the error handler

        Args:
            parent (object): parent object of the error handler, must support a 'write_to_log' method.

        Raises:
            NotImplementedError: fails, if 'write_to_log' method is not provided / available
        """
        
        self.parent = parent
        try:
            self.parent.write_to_log('eh', 'info', 'Initializing error handler (eh)')
        except AttributeError:
            raise NotImplementedError(f"[error handler] (fatal) The error handler's parent cannot write to log.")
        
        self.levels = {
            IowEsmErrorLevels.fatal : 0,
            IowEsmErrorLevels.warning : 1,
            IowEsmErrorLevels.info : 2
        }
        
        self.errors = {}
        for level in self.levels.keys():
            self.errors[level] = []
            
        self.log_file = backend.globals.root_dir + "/backend.log"
        
        self._read_log()
    
    def _read_log(self) -> None:
        """Read log file and parse errors
        """
        try:
            with open(self.log_file, 'r') as file:
                self.errors = json.load(file)
        except FileNotFoundError:
            pass
        
    def _write_log(self) -> None:
        """Write log errors to log file.
        """
        with open(self.log_file, 'w') as file:
            json.dump(self.errors, file)
        
        
    def report_stack(self, stack: tuple) -> None:
        """Report an error stack to the parent log.

        Args:
            stack (tuple): a tuple with the error stack, the last element is a string with the error stack
        """
        self.report_error('eh', 'fatal', 'received stack trace')
        self.report_error('eh', 'fatal', stack[-1])
    
    def report_error(self, source: str, level: str, info: str) -> bool:
        """Report an error or info to the parent log.

        Args:
            source (str): message source
            level (str): message level, should be in (info, warning, fatal, stdout)
            info (str): message

        Returns:
            bool: returns False if message level is unkown, True otherwise
        """
        if level not in self.levels.keys():
            self.parent.write_to_log(source, level, 'no such error level.')
            return False
        
        self._read_log()
        
        self.errors[level].append(info)
        self.parent.write_to_log(source, level, f"{info}")
        
        self._write_log()
        return True
        
    def check_for_error(self, level: str, info: str="") -> bool:
        """Check if a given level is in the log file.

        Args:
            level (str): message level
            info (str, optional): message string. Defaults to "".

        Returns:
            bool: returns True if message is in log, or if log is empty and given info is empty. False otherwise.
        """
        self._read_log()
        
        if not info:
            return not self.errors[level]
        
        return info in self.errors[level]
        
    def remove_from_log(self, source: str, level: str, info: str="") -> None:
        """Remove a record from the log file .

        Args:
            source (str): source for message, used as dummy here
            level (str): message level
            info (str, optional): message. If empty, the log at the level will be cleared entirely. Defaults to "".
        """
        self._read_log()
        
        if not info:
            self.errors[level] = []
        else:
            self.errors[level] = list(filter(lambda a: a != info, self.errors[level]))
            
        self._write_log()
        
    def get_log(self) -> dict:
        """Return the errors log.

        Returns:
            dict: error logs
        """
        return self.errors
