user_at_dest=$1
dest_folder=$2
prepare_before_run=${3:-false}
setup=${4:-""}

# adapt to your target's python module
python_module="python3/2023.01-gcc-11.2.0"

if [ ${prepare_before_run} == true ]; then
	echo ssh -t $user_at_dest "source /sw/etc/profile.levante; module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_mappings.py ${setup}"
	ssh -t $user_at_dest "source /sw/etc/profile.levante; module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_mappings.py ${setup}"
fi

echo ssh -t $user_at_dest "source /sw/etc/profile.levante; module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_jobscript.py ${setup}"
ssh -t $user_at_dest "source /sw/etc/profile.levante; module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_jobscript.py ${setup}"

if ssh $user_at_dest stat ${dest_folder}/input/${setup}/ENSEMBLE_MEMBERS \> /dev/null 2\>\&1
then  # ensemble run
	# download ensemble members for reference
	echo "Downloading ENSEMBLE_MEMBERS for local reference."
	rsync -ruv ${user_at_dest}:${dest_folder}/input/${setup}/ENSEMBLE_MEMBERS .

	# loop over the file remotely
	ssh -T $user_at_dest <<- EOF
	cd $dest_folder/scripts/run
	while read ensembleMember
	do
		ensembleSetup="\$(basename "\${ensembleMember}")"
		sbatch jobscript_\${ensembleSetup}
	done
	EOF
else  # single run
	# run standard if there is no ensemble member file
	echo ssh -t $user_at_dest "source /sw/etc/profile.levante; cd $dest_folder/scripts/run; sbatch jobscript_${setup}"
	ssh -t $user_at_dest "source /sw/etc/profile.levante; cd $dest_folder/scripts/run; sbatch jobscript_${setup}"
fi